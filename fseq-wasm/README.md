# fseq-wasm

WebAssembly wrapper for [fseq-nommer](../fseq-nommer/README.md). API _very_ unstable while I work out what is a good
interface to the browser.
