extern crate fseq_nommer;
extern crate nom;

use wasm_bindgen::prelude::*;
use fseq_nommer::{parse_fseq, FSEQ, Channel, FSEQError};
use core::result::Result;

#[wasm_bindgen]
pub struct FSEQWrapper(FSEQ);

fn format_error_message(input: FSEQError) -> js_sys::Error {
    match input {
        FSEQError::NotAnFSEQ => {js_sys::Error::new("File is not an FSEQ file")},
        FSEQError::UnsupportedVersion(version) => {js_sys::Error::new(&format!("FSEQ version {}.{} is not supported", version.major, version.minor))},
        FSEQError::IncorrectStructure(field, error) => {js_sys::Error::new(&format!("File structure did not meet expectations, received {:?} parsing field {:?}", error, field))},
        FSEQError::IncompleteData(field) => {js_sys::Error::new(&format!("Incomplete data reading {:?} field", field))},
        FSEQError::TextEncodingError => {js_sys::Error::new("Unable to decode variable headers")}
    }
}

#[wasm_bindgen]
impl FSEQWrapper {
    pub fn new(input: &[u8]) -> Result<FSEQWrapper,JsValue> {
        let result = parse_fseq(input);
        match result {
            Err(error) => {Err(format_error_message(error).into())},
            Ok(fseq) => { Ok(FSEQWrapper (fseq))}
        }
    }

    pub fn number_of_frames(&self) -> u32 {
        self.0.number_of_frames()
    }

    pub fn channel_count_per_frame(&self) -> u32 {
        self.0.channel_count_per_frame()
    }

    pub fn channels(self) -> *const Channel {
        self.0.channels()
    }
}

#[wasm_bindgen]
pub fn parse_fseq_status(input: &[u8]) -> Result<String,JsValue> {
    let result = parse_fseq(input);
    let status_string = match result {
        Err(error) => {format!("Parse failed {:?}", error)},
        Ok(_) => { "Parse successful".to_string()}
    };
    Ok(status_string)
}