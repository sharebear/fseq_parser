# fseq-parser

Rust library for parsing of fseq file format used by holiday lighting tools.

## What is an fseq file?

The fseq file format is a format for representing DMX channel data that is exported by [xLights](https://xlights.org/)
for consumption by multiple players, one example being [Falcon Pi Player](https://github.com/FalconChristmas/fpp) in
addition to multiple embedded players.

## Modules

This project is split into multiple parts, check out the local readme for more details;

* [fseq-nommer](fseq-nommer/README.md) : nom based parser for fseq files. At the time of writing it only supports v1
  format
* [fseq-inspector](fseq-inspector/README.md): command line tool for quick testing of the parser
* [fseq-wasm](fseq-wasm/README.md): module to expose the parser to web clients via WebAssembly
* [fseq-next](fseq-next/README.md): Next.js based web application to test the parser for anyone not wanting to install the
  command line tools
