# fseq-nommer

[Nom](https://github.com/Geal/nom) based parser for fseq files.

Currently only supports fseq v1, v2 support is planned as soon as I have some simple visualisations working.

The goal is to ensure that this will work in no-std environments so it will be usable in both wasm and embedded
environments.
