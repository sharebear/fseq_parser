use nom::bytes::complete::{tag, take_until};
use nom::{IResult, Err};
use nom::multi::count;
use nom::number::complete::{le_u16, le_u32, le_u8};
use nom::combinator::all_consuming;
use crate::FSEQField::{ChannelDataStart, MajorVersion, MinorVersion, IndexToFirstVariableHeader, ChannelCountPerFrame, StepTimeInMs, NumberOfFrames, BitFlagsReserved, UniverseCount, UniverseSize, Gamma, ColorEncoding, Reserved, ChannelData, VariableHeaderLength, VariableHeaderKey, VariableHeaderContent, VariableHeaderPadding};
use crate::FSEQError::{IncompleteData, TextEncodingError};
use nom::error::ErrorKind;
use core::fmt;
use nom::lib::std::fmt::{Formatter, Error};
use nom::lib::std::str::Utf8Error;

pub struct FSEQ {
    channel_data_start: u16,
    minor_version: u8,
    major_version: u8,
    index_to_first_variable_header: u16,
    channel_count_per_frame: u32,
    number_of_frames: u32,
    // FIXME: Can this get a time type?
    step_time_in_ms: u8,
    universe_count: u16,
    universe_size: u16,
    headers: Option<FSEQVariableHeaders>,
    channels: Vec<Channel>,
}

impl fmt::Debug for FSEQ {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        f.debug_struct("FSEQ")
            .field("channel_data_start", &self.channel_data_start)
            .field("minor_version", &self.minor_version)
            .field("major_version", &self.major_version)
            .field("index_to_first_variable_header", &self.index_to_first_variable_header)
            .field("channel_count_per_frame", &self.channel_count_per_frame)
            .field("number_of_frames", &self.number_of_frames)
            .field("step_time_in_ms", &self.step_time_in_ms)
            .field("universe_count", &self.universe_count)
            .field("universe_size", &self.universe_size)
            .field("headers", &self.headers)
            .field("channels.len", &self.channels.len())
            .finish()
    }
}

impl FSEQ {
    pub fn number_of_frames(&self) -> u32 {
        self.number_of_frames
    }

    pub fn channel_count_per_frame(&self) -> u32 {
        self.channel_count_per_frame
    }

    pub fn channels(self) -> *const Channel {
        self.channels.as_ptr()
    }
}

// TODO: Investigate if these should be newTypes instead
pub type Channel = u8;

fn parse_channel(input: &[u8]) -> IResult<&[u8], Channel, (&[u8], ErrorKind)> {
    let (remaining, channel) = le_u8(input)?;
    Ok((remaining, channel))
}

#[derive(Debug)]
pub struct FSEQVersion {
    pub major: u8,
    pub minor: u8,
}

#[derive(Debug)]
pub enum FSEQError {
    NotAnFSEQ,
    UnsupportedVersion(FSEQVersion),
    IncorrectStructure(FSEQField, ErrorKind),
    IncompleteData(FSEQField),
    TextEncodingError,
}

#[derive(Debug)]
pub enum FSEQField {
    ChannelDataStart,
    MinorVersion,
    MajorVersion,
    IndexToFirstVariableHeader,
    ChannelCountPerFrame,
    NumberOfFrames,
    StepTimeInMs,
    BitFlagsReserved,
    UniverseCount,
    UniverseSize,
    Gamma,
    ColorEncoding,
    Reserved,
    ChannelData,
    VariableHeaderLength,
    VariableHeaderKey,
    VariableHeaderContent,
    VariableHeaderPadding,
}

#[derive(Debug)]
pub struct FSEQVariableHeaders {
    pub media_filename: String,
    pub sequence_producer: String,
}

trait ErrorContext<Input, Output> {
    fn map_err_context(self, context: FSEQField) -> Result<(Input, Output), FSEQError>;
}

impl<Input, Output> ErrorContext<Input, Output> for IResult<Input, Output, (Input, ErrorKind)> {
    fn map_err_context(self, context: FSEQField) -> Result<(Input, Output), FSEQError> {
        self.map_err(|err| {
            match err {
                Err::Incomplete(_) => IncompleteData(context),
                Err::Error(e) => FSEQError::IncorrectStructure(context, e.1),
                Err::Failure(e) => FSEQError::IncorrectStructure(context, e.1),
            }
        }
        )
    }
}

impl From<Utf8Error> for FSEQError {
    fn from(_: Utf8Error) -> Self {
        TextEncodingError
    }
}

fn parse_headers(input: &[u8], channel_data_start: u16, index_to_first_variable_header: u16) -> Result<(&[u8], Option<FSEQVariableHeaders>), FSEQError> {
    if channel_data_start != index_to_first_variable_header {
        let (remaining, mp_length) = le_u16(input).map_err_context(VariableHeaderLength)?;
        let (remaining, _) = tag("mf")(remaining).map_err_context(VariableHeaderKey)?;
        let (remaining, media_filename) = take_until("\0")(remaining).map_err_context(VariableHeaderContent)?;
        let (remaining, _) = tag("\0")(remaining).map_err_context(VariableHeaderContent)?;
        let (remaining, sp_length) = le_u16(remaining).map_err_context(VariableHeaderLength)?;
        let (remaining, _) = tag("sp")(remaining).map_err_context(VariableHeaderKey)?;
        let (remaining, sequence_producer) = take_until("\0")(remaining).map_err_context(VariableHeaderContent)?;
        let (remaining, _) = tag("\0")(remaining).map_err_context(VariableHeaderContent)?;
        let padding_length = channel_data_start - index_to_first_variable_header - mp_length - sp_length;
        let (remaining, _) = count(tag("\0"), padding_length as usize)(remaining).map_err_context(VariableHeaderPadding)?;
        // Guessed that these are UTF-8, it's not documented
        let media_filename_str = core::str::from_utf8(media_filename)?.to_string();
        let sequence_producer_str = core::str::from_utf8(sequence_producer)?.to_string();
        Ok((remaining, Some(FSEQVariableHeaders {
            media_filename: media_filename_str,
            sequence_producer: sequence_producer_str,
        })))
    } else {
        Ok((input, Option::None))
    }
}

pub fn parse_fseq(input: &[u8]) -> Result<FSEQ, FSEQError> {
    let (remaining, _) = tag::<&str, &[u8], ()>("PSEQ")(input).map_err(|_| FSEQError::NotAnFSEQ)?;
    let (remaining, channel_data_start) = le_u16(remaining).map_err_context(ChannelDataStart)?;
    let (remaining, minor_version) = le_u8(remaining).map_err_context(MinorVersion)?;
    let (remaining, major_version) = le_u8(remaining).map_err_context(MajorVersion)?;
    if major_version != 1 {
        return Err(FSEQError::UnsupportedVersion(FSEQVersion { major: major_version, minor: minor_version }));
    }
    let (remaining, index_to_first_variable_header) = le_u16(remaining).map_err_context(IndexToFirstVariableHeader)?;
    let (remaining, channel_count_per_frame) = le_u32(remaining).map_err_context(ChannelCountPerFrame)?;
    let (remaining, number_of_frames) = le_u32(remaining).map_err_context(NumberOfFrames)?;
    let (remaining, step_time_in_ms) = le_u8(remaining).map_err_context(StepTimeInMs)?;
    let (remaining, _) = tag([0])(remaining).map_err_context(BitFlagsReserved)?;
    let (remaining, universe_count) = le_u16(remaining).map_err_context(UniverseCount)?;
    let (remaining, universe_size) = le_u16(remaining).map_err_context(UniverseSize)?;
    let (remaining, _) = tag([1])(remaining).map_err_context(Gamma)?;
    let (remaining, _) = tag([2])(remaining).map_err_context(ColorEncoding)?;
    let (remaining, _) = tag([0, 0])(remaining).map_err_context(Reserved)?;
    let (remaining, headers) = parse_headers(remaining, channel_data_start, index_to_first_variable_header)?;
    let (_, channels) = all_consuming(count(parse_channel, channel_count_per_frame as usize * number_of_frames as usize))(remaining).map_err_context(ChannelData)?;

    Ok(
        FSEQ {
            channel_data_start,
            minor_version,
            major_version,
            index_to_first_variable_header,
            channel_count_per_frame,
            number_of_frames,
            step_time_in_ms,
            universe_count,
            universe_size,
            headers,
            channels,
        }
    )
}