# fseq-next

Simple web application demonstrating a simple usage of the [fseq-wasm](../fseq-wasm/README.md) package.

## Test the running application

https://fseq-web.firebaseapp.com/

## Requirements

The webpack configuration executes `wasm-pack` in the wasm module to enable a short feedback loop even
when recompiling rust code. Therefore the development environment for [fseq-wasm](../fseq-wasm/README.md)
also needs to be in place, see the README of that project for details.

Other than that the only dependency for development is a working npm install.

## Usage

```
$ npm install
$ npm run dev
```

## Deploy of code

The application is deployed as a static generated site on Google Firebase Hosting. The following
commands do the necessary packaging and deploy of code.

```
$ rm -rf .next out
$ npm run build
$ npm run export
$ cd ..
$ firebase deploy
```
