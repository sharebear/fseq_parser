import {FSEQWrapper} from "../../fseq-wasm/pkg";

interface FSEQNotLoaded {
    state: "not-loaded";
}

interface FSEQLoading {
    state: "loading";
}

export const createFSEQLoading = (): FSEQLoading => {
    return {state: "loading"};
};

interface FSEQError {
    state: "error";
    detail: string;
}

export const createFSEQError = (detail: string): FSEQError => {
    return {state: "error", detail};
};

export interface FSEQMetadata {
    fseqPointer: FSEQWrapper;
    numberOfFrames: number;
    channelsPerFrame: number;
    channelData: Uint8Array;
}

export type FSEQLoaded = { state: "loaded"; currentFrame: number } & FSEQMetadata;

// Used interface param in this constructor due to number of positional params that would be easy to switch
export const createFSEQLoaded = (metadata: FSEQMetadata, currentFrame: number): FSEQLoaded => {
    return {...metadata, state: "loaded", currentFrame};
};

export type FSEQState = FSEQNotLoaded | FSEQLoading | FSEQError | FSEQLoaded;

export const isFSEQNotLoaded = (input: FSEQState): input is FSEQNotLoaded => {
    return input.state === "not-loaded";
};

export const isFSEQLoading = (input: FSEQState): input is FSEQLoading => {
    return input.state === "loading";
};

export const isFSEQLoaded = (input: FSEQState): input is FSEQLoaded => {
    return input.state === "loaded";
};

export const isFSEQError = (input: FSEQState): input is FSEQError => {
    return input.state === "error";
};
