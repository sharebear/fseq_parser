import {FSEQWrapper} from "../../fseq-wasm/pkg";

interface WASMRefsLoading {
    status: "loading";
}

export const createWasmLoading = (): WASMRefsLoading => {
    return {status: "loading"};
};

interface WASMRefsError {
    status: "error";
    detail: string;
}

export const createWasmError = (detail: string): WASMRefsError => {
    return {status: "error", detail};
};

export interface WASMRefsLoaded {
    status: "loaded";
    memory: WebAssembly.Memory;
    fseqClass: typeof FSEQWrapper;
}

export const createWasmLoaded = (fseqClass: typeof FSEQWrapper, memory: WebAssembly.Memory): WASMRefsLoaded => {
    return {status: "loaded", fseqClass, memory}
};

export type WASMRefs = WASMRefsLoading | WASMRefsError | WASMRefsLoaded;

export const isWasmLoading = (input: WASMRefs): input is WASMRefsLoading => {
    return input.status === "loading";
};

export const isWasmError = (input: WASMRefs): input is WASMRefsError => {
    return input.status === "error";
};

export const isWasmLoaded = (input: WASMRefs): input is WASMRefsLoaded => {
    return input.status === "loaded";
};
