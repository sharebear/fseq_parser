import {
    createFSEQError,
    createFSEQLoaded,
    createFSEQLoading,
    FSEQMetadata,
    FSEQState,
    isFSEQLoaded,
    isFSEQLoading,
    isFSEQNotLoaded
} from "./fseq";
import {createWasmError, createWasmLoaded, createWasmLoading, isWasmLoaded, WASMRefs} from "./wasm-refs";
import {FSEQWrapper} from "../../fseq-wasm/pkg";
import {
    addPixelStrin,
    createDefaultStringModels,
    removeString,
    StringModels,
    updatePixelCount,
    updateStartChannel
} from "./string-config";

export interface State {
    wasmRefs: WASMRefs;
    fseq: FSEQState;
    stringModels: StringModels;
}

export const initialState = (): State => {
    return {
        wasmRefs: createWasmLoading(),
        fseq: {state: "not-loaded"},
        stringModels: createDefaultStringModels()
    };
};

const incrementFrame = (state: State): State => {
    if (isFSEQLoaded(state.fseq)) {
        return {...state, fseq: createFSEQLoaded(state.fseq, state.fseq.currentFrame + 1)};
    } else {
        throw new Error("FIXME dammit! I need an application error state");
    }
};

const decrementFrame = (state: State): State => {
    if (isFSEQLoaded(state.fseq)) {
        return {...state, fseq: createFSEQLoaded(state.fseq, state.fseq.currentFrame - 1)};
    } else {
        throw new Error("FIXME dammit! I need an application error state");
    }
};

const setFrame = (state: State, newFrame: number): State => {
    if (isFSEQLoaded(state.fseq)) {
        return {...state, fseq: createFSEQLoaded(state.fseq, newFrame)};
    } else {
        throw new Error("FIXME dammit! I need an application error state");
    }
};

export const isFileSelectorDisabled = (state: State): boolean => {
    return (!isWasmLoaded(state.wasmRefs)) || isFSEQLoading(state.fseq);
};

export const isWaitingForFile = (state: State): boolean => {
    return isWasmLoaded(state.wasmRefs) && isFSEQNotLoaded(state.fseq);
};

interface WASMLoaded {
    type: "wasm-loaded";
    fseqClass: typeof FSEQWrapper;
    memory: WebAssembly.Memory;
}

export const createWASMLoaded = (fseqClass: typeof FSEQWrapper, memory: WebAssembly.Memory): WASMLoaded => {
    return {type: "wasm-loaded", fseqClass, memory};
};

interface WASMErrored {
    type: "wasm-errored";
    e: unknown;
}

export const createWASMErrored = (e: unknown): WASMErrored => {
    return {type: "wasm-errored", e};
};

interface FileChosen {
    type: "file-chosen";
}

export const createFileChosen = (): FileChosen => {
    return {type: "file-chosen"};
};

interface FileSuccess {
    type: "file-success";
    metadata: FSEQMetadata;
}

export const createFileSuccess = (metadata: FSEQMetadata): FileSuccess => {
    return {type: "file-success", metadata};
};

interface FileFailed {
    type: "file-failed";
    e: unknown;
}

export const createFileFailed = (e: unknown): FileFailed => {
    return {type: "file-failed", e};
};

interface IncrementFrame {
    type: "increment-frame";
}

export const createIncrementFrame = (): IncrementFrame => {
    return {type: "increment-frame"};
};

interface DecrementFrame {
    type: "decrement-frame";
}

export const createDecrementFrame = (): DecrementFrame => {
    return {type: "decrement-frame"};
};

interface SetFrame {
    type: "set-frame";
    newFrame: number;
}

export const createSetFrame = (newFrame: number): SetFrame => {
    return {type: "set-frame", newFrame};
};

interface SetStartChannel {
    type: "set-start-channel";
    index: number;
    newValue: number;
}

export const createSetStartChannel = (index: number, newValue: number): SetStartChannel => {
    return {type: "set-start-channel", index, newValue};
};

interface SetPixelCount {
    type: "set-pixel-count";
    index: number;
    newValue: number;
}

export const createSetPixelCount = (index: number, newValue: number): SetPixelCount => {
    return {type: "set-pixel-count", index, newValue};
};

interface AddPixelString {
    type: "add-pixel-string";
}

export const createAddPixelString = (): AddPixelString => {
    return {type: "add-pixel-string"};
};

interface RemovePixelString {
    type: "remove-pixel-string";
    index: number;
}

export const createRemovePixelString = (index: number): RemovePixelString => {
    return {type: "remove-pixel-string", index};
};

type AppAction = WASMLoaded
    | WASMErrored
    | FileChosen
    | FileSuccess
    | FileFailed
    | IncrementFrame
    | DecrementFrame
    | SetFrame
    | SetStartChannel
    | SetPixelCount
    | AddPixelString
    | RemovePixelString;

export const reducer = (state: State, action: AppAction): State => {
    switch (action.type) {
        case "wasm-loaded":
            return {...(state), wasmRefs: createWasmLoaded(action.fseqClass, action.memory)};
        case "wasm-errored":
            return {...(state), wasmRefs: createWasmError(action.e.toString())};
        case "file-chosen":
            return {...(state), fseq: createFSEQLoading()};
        case "file-success":
            return {...(state), fseq: createFSEQLoaded(action.metadata, 0)};
        case "file-failed":
            return {...(state), fseq: createFSEQError(action.e.toString())};
        case "increment-frame":
            return incrementFrame(state);
        case "decrement-frame":
            return decrementFrame(state);
        case "set-frame":
            return setFrame(state, action.newFrame);
        case "set-start-channel":
            return {...(state), stringModels: updateStartChannel(state.stringModels, action.index, action.newValue)};
        case "set-pixel-count":
            return {...(state), stringModels: updatePixelCount(state.stringModels, action.index, action.newValue)};
        case "add-pixel-string":
            return {...(state), stringModels: addPixelStrin(state.stringModels)};
        case "remove-pixel-string":
            return {...(state), stringModels: removeString(state.stringModels, action.index)};
        default:
            throw new Error("FIXME: Handle technical error better, although compiler should catch this");
    }
};
