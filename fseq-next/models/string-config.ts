export interface PixelString {
    startChannel: number;
    pixelCount: number;
}

export interface StringModels {
    strings: PixelString[];
}

export const createDefaultStringModels = (): StringModels => {
    return {strings: [{startChannel: 1, pixelCount: 50}]};
};

export const calculateLastChannel = (pixelString: PixelString): number => {
    return pixelString.pixelCount * 3 + pixelString.startChannel - 1;
};

export const updateStartChannel = (current: StringModels, index: number, newValue: number): StringModels => {
    const currentString = current.strings[index];
    const updatedString = {...currentString, startChannel: newValue};
    const updatedArray = current.strings.map((item, i) => i === index ? updatedString : item);
    return {strings: updatedArray};
};

export const updatePixelCount = (current: StringModels, index: number, newValue: number): StringModels => {
    const currentString = current.strings[index];
    const updatedString = {...currentString, pixelCount: newValue};
    const updatedArray = current.strings.map((item, i) => i === index ? updatedString : item);
    return {strings: updatedArray};
};

export const addPixelStrin = (current: StringModels): StringModels => {
    // Copy Array so operation is effectively immutable
    const updatedArray = current.strings.map(item => item);
    updatedArray.push({
        startChannel: calculateLastChannel(current.strings[current.strings.length - 1]) + 1,
        pixelCount: 50
    });
    return {strings: updatedArray};
};

export const removeString = (current: StringModels, index: number): StringModels => {
    // Copy Array so operation is effectively immutable
    const updatedArray = current.strings.flatMap((item, i) => i === index ? [] : [item]);
    return {strings: updatedArray};
};
