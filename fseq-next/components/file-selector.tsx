import Button from "@material-ui/core/Button";
import React, {FunctionComponent, useRef} from "react";
import {Grid, List, ListItem, ListItemText, ListSubheader, Paper} from "@material-ui/core";

interface FileSelectorProps {
    disabled: boolean;
    onFileSelected: (file: File) => void;
    onExampleSelected: (filename: string) => void;
}

const FileSelector: FunctionComponent<FileSelectorProps> = (props: FileSelectorProps) => {
    const fileUploadEl = useRef(null);

    return <Grid container direction="column" justify="flex-start" spacing={1}>
        <Grid item>
            <Button
                variant="contained"
                color="secondary"
                onClick={(): void => fileUploadEl.current.click()}
                disabled={props.disabled}
                fullWidth
            >
                Open FSEQ file
                <input
                    ref={fileUploadEl}
                    type="file"
                    accept=".fseq"
                    style={{display: "none"}}
                    onChange={(e): void => props.onFileSelected(e.target.files[0])}
                />
            </Button>
        </Grid>
        <Grid item>
            <Paper>
                <List dense={true} subheader={<ListSubheader disableSticky>or choose an example file below</ListSubheader>}>
                    <ListItem button={true} disabled={props.disabled}
                              onClick={(): void => props.onExampleSelected('/examples/build_failed.fseq')}>
                        <ListItemText primary="FSEQ v1 Animation" secondary="111 channels (37 pixels)"/>
                    </ListItem>
                    <ListItem button={true} disabled={props.disabled}
                              onClick={(): void => props.onExampleSelected('/examples/CarolOfTheBellsv1.fseq')}>
                        <ListItemText primary="FSEQ v1 Sequence" secondary="600 channels (200 pixels)"/>
                    </ListItem>
                    <ListItem button={true} disabled={props.disabled}
                              onClick={(): void => props.onExampleSelected('/examples/StaticAnimation.fseq')}>
                        <ListItemText primary="FSEQ v2 Animation" secondary="Not currently supported"/>
                    </ListItem>
                </List>
            </Paper>
        </Grid>
    </Grid>
};

export default FileSelector;
