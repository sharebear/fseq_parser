/* eslint-disable @typescript-eslint/explicit-function-return-type */

/* Polyfill stolen from/inspired by https://github.com/eligrey/Blob.js */

declare global {
    interface Blob {
        arrayBuffer();
    }

    interface Response {
        arrayBuffer();
    }
}

function promisify(obj) {
    return new Promise(function(resolve, reject) {
        obj.onload =
            obj.onerror = function(evt) {
                obj.onload =
                    obj.onerror = null;

                evt.type === 'load'
                    ? resolve(obj.result || obj)
                    : reject(new Error('Failed to read the blob/file'))
            }
    })
}

export const applyArrayBufferPolyfill = () => {
    if (!Blob.prototype.arrayBuffer) {
        console.log("Applying polyfill of Blob.arrayBuffer");
        Blob.prototype.arrayBuffer = function arrayBuffer() {
            console.log("Using polyfill of Blob.arrayBuffer");
            const fr = new FileReader();
            fr.readAsArrayBuffer(this);
            return promisify(fr)
        }
    }
};