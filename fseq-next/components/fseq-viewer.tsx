import {FSEQLoaded} from "../models/fseq";
import Pixel from "./pixel";
import React, {FunctionComponent} from "react";
import {Grid, IconButton, Slider, Typography} from "@material-ui/core";
import {NavigateBefore, NavigateNext} from "@material-ui/icons";
import {StringModels} from "../models/string-config";

const getRed = (fseq: FSEQLoaded, startChannel: number, index: number): number => {
    return fseq.channelData[(fseq.currentFrame * fseq.channelsPerFrame) + (startChannel - 1) + (index * 3)];
};

const getGreen = (fseq: FSEQLoaded, startChannel: number, index: number): number => {
    return fseq.channelData[(fseq.currentFrame * fseq.channelsPerFrame) + (startChannel - 1) + (index * 3) + 1];
};

const getBlue = (fseq: FSEQLoaded, startChannel: number, index: number): number => {
    return fseq.channelData[(fseq.currentFrame * fseq.channelsPerFrame) + (startChannel - 1) + (index * 3) + 2];
};

const maxChannel = (startChannel: number, index: number): number => {
    return (startChannel - 2) + ((index + 1) * 3);
};

interface StringViewProps {
    fseq: FSEQLoaded;
    width: number;
    startChannel: number;
}

const StringView: FunctionComponent<StringViewProps> = (props: StringViewProps) => {
    return (
        <g>
            {Array.from(Array(props.width).keys())
                .flatMap(index => maxChannel(props.startChannel, index) < props.fseq.channelsPerFrame ?
                    [<g key={index} transform={`translate(${10 * index + 5}, 5)`}>
                        <Pixel red={getRed(props.fseq, props.startChannel, index)}
                               green={getGreen(props.fseq, props.startChannel, index)}
                               blue={getBlue(props.fseq, props.startChannel, index)}/>
                    </g>] : []
                )
            }
        </g>
    )
};

interface FSEQViewerProps {
    fseq: FSEQLoaded;
    stringModels: StringModels;
    onPrevious: () => void;
    onNext: () => void;
    onSlide: (newFrame: number) => void;
}

const FSEQViewer: FunctionComponent<FSEQViewerProps> = (props: FSEQViewerProps) => {
    const widthInPixels = Math.max(...props.stringModels.strings.map((s) => s.pixelCount));

    const heightInPixels = props.stringModels.strings.length * 10;

    const svgViewBox = `0 0 ${widthInPixels * 10} ${heightInPixels}`;

    return (<div>
        <Grid container justify="space-between" alignItems="center">
            <IconButton onClick={(): void => props.onPrevious()}
                        disabled={props.fseq.currentFrame === 0}><NavigateBefore/></IconButton>
            <Grid item xs>
                <Typography
                    align="center">Frame {props.fseq.currentFrame + 1} of {props.fseq.numberOfFrames}</Typography>
                <Slider min={1} max={props.fseq.numberOfFrames} value={props.fseq.currentFrame + 1}
                        onChange={(_, newFrame: number): void => props.onSlide(newFrame - 1)}/>
            </Grid>
            <IconButton onClick={(): void => props.onNext()}
                        disabled={props.fseq.currentFrame + 1 >= props.fseq.numberOfFrames}><NavigateNext/></IconButton>
        </Grid>
        <svg style={{background: "black"}} viewBox={svgViewBox}>
            {props.stringModels.strings.map((pixelString, index) => <g key={index}
                                                                       transform={`translate(0, ${10 * index})`}>
                <StringView fseq={props.fseq} startChannel={pixelString.startChannel}
                            width={Math.min(widthInPixels, pixelString.pixelCount)}/></g>)}
        </svg>
    </div>);
};

export default FSEQViewer;
