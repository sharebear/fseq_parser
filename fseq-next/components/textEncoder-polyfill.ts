export const applyTextEncoderPolyfill = async (): Promise<void> => {
    if (!window.TextEncoder) {
        // Needed for older versions of Edge (so could probably soon be skipped):
        const encoding = await import('text-encoding-utf-8');
        window.TextEncoder = encoding.TextEncoder;
        window.TextDecoder = encoding.TextDecoder;
    }
};
