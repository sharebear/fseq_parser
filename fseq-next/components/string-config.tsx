import React, {FunctionComponent} from "react";
import {
    Grid,
    IconButton,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    ListSubheader,
    NoSsr,
    TextField
} from "@material-ui/core";
import {Add, DeleteForever} from "@material-ui/icons";
import {calculateLastChannel, PixelString, StringModels} from "../models/string-config";


interface StringConfigItemProps {
    index: number;
    pixelString: PixelString;
    canDelete: boolean;
    onStartChannelChanged: (newValue: number) => void;
    onPixelCountChanged: (newValue: number) => void;
    onStringDeleted: () => void;
}

const StringConfigItem: FunctionComponent<StringConfigItemProps> = (props: StringConfigItemProps) => {
    return (
        <ListItem>
            <ListItemText>
                <Grid container justify="center" spacing={1}>
                    <Grid item xs={10} xl={3}>
                        <TextField id={`start-channel-${props.index}`} label="Start channel" variant="outlined"
                                   value={props.pixelString.startChannel}
                                   onChange={(e): void => props.onStartChannelChanged(parseInt(e.target.value, 10))}
                                   type="number" margin="dense"/>
                    </Grid>
                    <Grid item xs={10} xl={3}>
                        <TextField id={`pixel-count-${props.index}`} label="Pixel count" variant="outlined"
                                   value={props.pixelString.pixelCount}
                                   onChange={(e): void => props.onPixelCountChanged(parseInt(e.target.value, 10))}
                                   type="number" margin="dense"/>
                    </Grid>
                    <Grid item xs={10} xl={3}>
                        <TextField id={`end-channel-${props.index}`} label="End channel" variant="outlined"
                                   value={calculateLastChannel(props.pixelString)}
                                   type="number" margin="dense" disabled/>
                    </Grid></Grid>
            </ListItemText>
            <ListItemSecondaryAction>
                {props.canDelete && <IconButton onClick={props.onStringDeleted}><DeleteForever/></IconButton>}
            </ListItemSecondaryAction>
        </ListItem>
    );
};

interface StringConfigProps {
    stringModels: StringModels;
    onStartChannelChanged: (index: number, newValue: number) => void;
    onPixelCountChanged: (index: number, newValue: number) => void;
    onStringAdded: () => void;
    onStringDeleted: (index: number) => void;
}

const StringConfig: FunctionComponent<StringConfigProps> = (props: StringConfigProps) => {
    return (<NoSsr>
        <List dense subheader={<ListSubheader disableSticky>Strings</ListSubheader>}>
            {props.stringModels.strings.map((pixelString, index) => <StringConfigItem key={index} index={index}
                                                                                      pixelString={pixelString}
                                                                                      canDelete={props.stringModels.strings.length > 1}
                                                                                      onStartChannelChanged={(newValue): void => props.onStartChannelChanged(index, newValue)}
                                                                                      onPixelCountChanged={(newValue): void => props.onPixelCountChanged(index, newValue)}
                                                                                      onStringDeleted={(): void => props.onStringDeleted(index)}
            />)}
            <ListItem><Grid container justify="center">
                <IconButton onClick={props.onStringAdded}><Add/></IconButton>
            </Grid></ListItem>
        </List></NoSsr>);
};

export default StringConfig;
