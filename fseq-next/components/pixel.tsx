import {FunctionComponent} from "react";

interface PixelProps {
    red: number;
    green: number;
    blue: number;
}

const Pixel: FunctionComponent<PixelProps> = (props: PixelProps) => {
    return (
        <circle r="3" fill={`rgb(${props.red}, ${props.green}, ${props.blue})`}/>
    );
};

export default Pixel;