import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import teal from '@material-ui/core/colors/teal';
import {AppBar, CssBaseline, Link, Toolbar, Typography} from "@material-ui/core";
import React, {FunctionComponent} from "react";
import {AppProps} from "next/app";
import NextLink from "next/link";
import {useRouter} from "next/router";

const theme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: {
            main: '#e53935',
        },
    },
});

const MyApp: FunctionComponent<AppProps> = ({Component, pageProps}: AppProps) => {
    const router = useRouter();

    return <ThemeProvider theme={theme}>
        <CssBaseline/>
        <AppBar position="relative">
            <Toolbar>
                <Typography variant="h5" component="h1" style={{flexGrow: 1}}>FSEQ Viewer</Typography>
                {router.pathname === "/" && <NextLink href="/about" passHref={true}>
                    <Link color="inherit">What is this?</Link>
                </NextLink>}
                {router.pathname !== "/" && <NextLink href="/" passHref={true}>
                    <Link color="inherit">Return to application</Link>
                </NextLink>}
            </Toolbar>
        </AppBar>
        <Component {...pageProps} />
    </ThemeProvider>
};

export default MyApp