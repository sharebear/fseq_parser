import React, {useEffect, useReducer} from "react";
import {FSEQWrapper} from "../../fseq-wasm/pkg";
import {
    createAddPixelString,
    createDecrementFrame,
    createFileChosen,
    createFileFailed,
    createFileSuccess,
    createIncrementFrame,
    createRemovePixelString,
    createSetFrame,
    createSetPixelCount,
    createSetStartChannel,
    createWASMErrored,
    createWASMLoaded,
    initialState,
    isFileSelectorDisabled,
    isWaitingForFile,
    reducer
} from "../models/app-state";
import {Grid, Paper, Typography, useTheme} from "@material-ui/core";
import {isWasmError, isWasmLoading, WASMRefsLoaded} from "../models/wasm-refs";
import {FSEQMetadata, isFSEQError, isFSEQLoaded, isFSEQLoading} from "../models/fseq";
import FSEQViewer from "../components/fseq-viewer";
import FileSelector from "../components/file-selector";
import {NextPage} from "next";
import {applyArrayBufferPolyfill} from "../components/arrayBuffer-polyfill";
import {applyTextEncoderPolyfill} from "../components/textEncoder-polyfill";
import StringConfig from "../components/string-config";


const Index: NextPage = () => {
    const theme = useTheme();
    const [state, dispatch] = useReducer(reducer, initialState());

    useEffect(() => {
        const loadWasm = async (): Promise<void> => {
            try {
                const {FSEQWrapper} = await import("../../fseq-wasm/pkg");
                const {memory} = await import("../../fseq-wasm/pkg/index_bg");
                dispatch(createWASMLoaded(FSEQWrapper, memory));
            } catch (e) {
                dispatch(createWASMErrored(e));
            }
        };

        loadWasm();
    }, []);

    useEffect(() => {
        applyArrayBufferPolyfill();
    }, []);

    useEffect(() => {
        (async (): Promise<void> => await applyTextEncoderPolyfill())();
    }, []);

    const parseData = async (input: File | Response, wasmRefs: WASMRefsLoaded): Promise<FSEQMetadata> => {
        // any type here as typescript definitions don't have arrayBuffer on File/Blob
        const arrayBuffer = await input.arrayBuffer();
        const array = new Uint8Array(arrayBuffer);
        const result: FSEQWrapper = wasmRefs.fseqClass.new(array);
        const numberOfFrames = result.number_of_frames();
        const channelCountPerFrame = result.channel_count_per_frame();
        const channelPointer = result.channels();
        const channelData = new Uint8Array(wasmRefs.memory.buffer, channelPointer, channelCountPerFrame * numberOfFrames);
        return {
            fseqPointer: result,
            numberOfFrames: numberOfFrames,
            channelsPerFrame: channelCountPerFrame,
            channelData: channelData
        };
    };

    const handleFileChanged = async (file: File): Promise<void> => {
        // If check required for narrowing of wasmRefs
        if (state.wasmRefs.status === "loaded") {
            dispatch(createFileChosen());
            try {
                const fseqMetadata = await parseData(file, state.wasmRefs);
                dispatch(createFileSuccess(fseqMetadata));
            } catch (e) {
                dispatch(createFileFailed(e));
            }
        } else {
            dispatch(createFileFailed("Application in inconsistent state"));
        }
    };

    const handleExampleSelected = async (filename: string): Promise<void> => {
        // If check required for narrowing of wasmRefs
        if (state.wasmRefs.status === "loaded") {
            dispatch(createFileChosen());
            try {
                const response = await fetch(filename);
                if (response.ok) {
                    const fseqMetadata = await parseData(response, state.wasmRefs);
                    dispatch(createFileSuccess(fseqMetadata));
                } else {
                    const error = `HTTP Error ${response.status} retrieving ${response.url}`;
                    dispatch(createFileFailed(error));
                }
            } catch (e) {
                dispatch(createFileFailed(e));
            }
        } else {
            dispatch(createFileFailed("Application UI in inconsistent state"));
        }
    };

    return (
        <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="flex-start"
            className="content"
        >
            <Grid item xs={12} sm={7}>
                <Paper>
                    {isWasmLoading(state.wasmRefs) && <Typography>Loading application files</Typography>}
                    {isWasmError(state.wasmRefs) &&
                    <Typography>Error occurred loading application. Reason: {state.wasmRefs.detail}</Typography>}
                    {isWaitingForFile(state) && <Typography>Please select a file</Typography>}
                    {isFSEQLoading(state.fseq) && <Typography>Loading file</Typography>}
                    {isFSEQError(state.fseq) &&
                    <Typography>Failed to load file. Reason: {state.fseq.detail}</Typography>}
                    {isFSEQLoaded(state.fseq) &&
                    <FSEQViewer
                        fseq={state.fseq}
                        stringModels={state.stringModels}
                        onPrevious={(): void => dispatch(createDecrementFrame())}
                        onNext={(): void => {dispatch(createIncrementFrame())}}
                        onSlide={(newFrame: number): void => {dispatch(createSetFrame(newFrame));}}/>}
                </Paper>
            </Grid>
            <Grid item xs={12} sm={3}>
                <FileSelector disabled={isFileSelectorDisabled(state)}
                              onFileSelected={handleFileChanged}
                              onExampleSelected={handleExampleSelected}/>
                <Paper>
                    <StringConfig stringModels={state.stringModels}
                                  onStartChannelChanged={(index: number, newValue: number): void => {dispatch(createSetStartChannel(index, newValue));}}
                                  onPixelCountChanged={(index: number, newValue: number): void => {dispatch(createSetPixelCount(index, newValue));}}
                                  onStringAdded={(): void => {dispatch(createAddPixelString());}}
                                  onStringDeleted={(index: number): void => {dispatch(createRemovePixelString(index));}}
                    />
                </Paper>
            </Grid>
            <style jsx global>{`
                .content {
                  padding-top: ${theme.spacing(2)}px;
                }
             `}</style>
        </Grid>
    );
};

export default Index;
