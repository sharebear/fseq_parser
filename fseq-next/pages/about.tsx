import React from "react";
import {Card, CardContent, CardHeader, Grid, Link, Typography} from "@material-ui/core";
import {NextPage} from "next";


const About: NextPage = () => {
    return (
        <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="flex-start"
            className="content"
        >
            <Grid item xs={12} sm={10} md={6}>
                <Card>
                    <CardHeader title="About"/>
                    <CardContent>
                        <Typography gutterBottom variant="h6">What is this?</Typography>
                        <Typography gutterBottom>A small hobby project of mine for showing the contents of an FSEQ file on a web
                            page</Typography>
                        <Typography gutterBottom variant="h6">What is an FSEQ file?</Typography>
                        <Typography gutterBottom>
                            The FSEQ file format is used by popular tools in the holiday lighting community. More
                            specifically <Link href="https://xlights.org/">xLights</Link> can be used to export FSEQ
                            files for consumption by player software such as <Link href="https://github.com/FalconChristmas/fpp">Falcon Player</Link>.
                        </Typography>
                        <Typography gutterBottom variant="h6">But why?</Typography>
                        <Typography gutterBottom>
                            The last couple of years I have taken an interest in using these holiday lighting tools to
                            create some fun projects. In the build up to Christmas 2018 I created a <Link href="https://blog.sharebear.co.uk/2019/01/office-christmas-tree-project-intro/">Christmas tree for the office</Link> and
                            last Christmas I put some effort into the sequencing side of things with a <Link href="https://vimeo.com/382155979">Christmas tree at home</Link>.
                        </Typography>
                        <Typography gutterBottom>
                            Parallel to this I have an interest in programming <Link href="https://www.rust-lang.org/">Rust</Link> and I think it would be cool to do
                            something related running Rust on an embedded platform as I don&apos;t need the full power of a
                            Raspberry Pi to run my small projects. There are already Arduino based projects in the
                            community, but then I wouldn&apos;t have the fun of doing it myself and the reliability that comes
                            from Rust.
                        </Typography>
                        <Typography gutterBottom>
                            Once I had the first iteration of the parser I thought that a fun way to show it off would
                            be to create a Web Assembly module for it and make a small website to visualise the results
                            of the parsing. This gave me the opportunity to practice using the Web Assembly bindings in
                            Rust, and learning some web based tools I&apos;ve been wanting to play with for a while such
                            as <Link href="https://nextjs.org/">Next.js</Link> and <Link href="https://reactjs.org/">React</Link>.
                        </Typography>
                        <Typography gutterBottom variant="h6">What is it useful for?</Typography>
                        <Typography gutterBottom>
                            Not much, it&apos;s mostly a toy for me to practice with various development tools but
                            there is some potential that some parts could form the basis of a more interesting project later.
                        </Typography>
                        <Typography gutterBottom variant="h6">Can I see the code?</Typography>
                        <Typography gutterBottom>
                            Of course! The code for the core parser, web assembly module, Next.js site and a command
                            line tool are grouped in a single gitlab repository <Link href="https://gitlab.com/sharebear/fseq_parser/">https://gitlab.com/sharebear/fseq_parser/</Link>.
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
};

export default About;
