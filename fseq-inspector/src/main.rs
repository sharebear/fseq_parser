extern crate fseq_nommer;

use std::fs::read;
use fseq_nommer::parse_fseq;


fn main() {
    let file = read("/home/jshare/Documents/xLights/upstairsTree/CarolOfTheBells.fseq").unwrap();
    //let file = read("/home/jshare/Documents/xLights/christmasTree/pulsing_playtime.fseq").unwrap();
    let fseq = parse_fseq(file.as_ref()).unwrap();
    println!("The result is {:#?}", fseq);
}
